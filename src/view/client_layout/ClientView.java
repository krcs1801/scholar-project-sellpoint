package view.client_layout;

import controller.ClientController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;

public class ClientView extends JPanel {
    ClientView() {
        super(new GridLayout(1, 0));

        JTable table = null;
        try {
            table = new JTable(new ClientView.MyTableModel());
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_ALL_COLUMNS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        table.setPreferredScrollableViewportSize(new Dimension(1800, 1100));
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }

    class MyTableModel extends AbstractTableModel {
        private String[] columnNames = {"Nombre", "Teléfono", "Domicilio", "Registro"};

        private Object[][] data = ClientController.read();

        MyTableModel() throws Exception {
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return 20;//data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }


        public boolean isCellEditable(int row, int col) {

            return true;
        }


        @Override
        public void fireTableCellUpdated(int row, int column) {
            super.fireTableCellUpdated(row, column);
        }


    }


    public static void createAndShowGUI(int x, int y) {
        JFrame frame = new JFrame("Clientes");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ClientView newContentPane = new ClientView();
        frame.setContentPane(newContentPane);
        frame.setLocation(x, y);
        frame.setSize(400, 400);
        frame.setVisible(true);

    }
}
