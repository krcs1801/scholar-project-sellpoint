package view.client_layout;

import controller.ClientController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ClientEdit extends JDialog {
    private JTextField tfName;
    private JTextField tfId;
    private JTextField tfPhone;
    private JTextField tfAddress;


    private JLabel lbPhone;
    private JLabel lbAddress;

    private JLabel lnName;
    private JLabel lbId;


    public ClientEdit(Frame parent) {
        super(parent, "Editar", true);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;
        lbId = new JLabel("ID: ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbId, cs);

        tfId = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(tfId, cs);

        lnName = new JLabel("Nombre: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lnName, cs);

        tfName = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfName, cs);

        lbPhone = new JLabel("Teléfono: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbPhone, cs);

        tfPhone = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 5;
        panel.add(tfPhone, cs);

        lbAddress = new JLabel("Domicilio: ");
        cs.gridx = 0;
        cs.gridy = 4;
        cs.gridwidth = 1;
        panel.add(lbAddress, cs);

        tfAddress = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 4;
        cs.gridwidth = 5;
        panel.add(tfAddress, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnSubmit = new JButton("Aceptar");
        JButton btnErase = new JButton("Limpiar");

        JPanel bp = new JPanel();
        bp.add(btnSubmit);
        // bp.add(btnErase);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        btnSubmit.addActionListener(e -> {
            try {
                ClientController.update(getTfId(), getTfName(), getTfAddress(), getTfPhone());
                JOptionPane.showMessageDialog(ClientEdit.this,
                        "El cliente ah sido editado exitosamente!",
                        "Editar cliente",
                        JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });
        pack();
        setLocation(400, 0);
        setLocationRelativeTo(null);
    }


    public String getTfName() {
        return tfName.getText();
    }

    public int getTfId() {
        return Integer.parseInt(tfId.getText());
    }

    public String getTfAddress() {
        return tfAddress.getText();
    }

    public String getTfPhone() {
        return tfPhone.getText();
    }

}
