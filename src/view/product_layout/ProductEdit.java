package view.product_layout;

import controller.ProductController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProductEdit extends JDialog {
    private JTextField tfProduct;
    private JTextField tfId;
    private JTextField tfBuyCost;
    private JTextField tfSellPrice;
    private JTextField tfInventory;
    private JTextField tfSupplier;


    private JTextField tfStock;

    private JLabel lbBuyCost;
    private JLabel lbSellPrice;
    private JLabel lbStock;
    private JLabel lbInventory;
    private JLabel lbProduct;
    private JLabel lbId;
    private JLabel lbSupplier;


    public ProductEdit(Frame parent) {
        super(parent, "Editar", true);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;
        lbId = new JLabel("ID: ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbId, cs);

        tfId = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(tfId, cs);

        lbProduct = new JLabel("Producto: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lbProduct, cs);

        tfProduct = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfProduct, cs);

        lbBuyCost = new JLabel("Costo: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbBuyCost, cs);

        tfBuyCost = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 5;
        panel.add(tfBuyCost, cs);

        lbSellPrice = new JLabel("Precio: ");
        cs.gridx = 0;
        cs.gridy = 4;
        cs.gridwidth = 1;
        panel.add(lbSellPrice, cs);

        tfSellPrice = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 4;
        cs.gridwidth = 5;
        panel.add(tfSellPrice, cs);

        lbInventory = new JLabel("Existencia: ");
        cs.gridx = 0;
        cs.gridy = 5;
        cs.gridwidth = 1;
        panel.add(lbInventory, cs);

        tfInventory = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 5;
        cs.gridwidth = 5;
        panel.add(tfInventory, cs);

        lbStock = new JLabel("NR: ");
        cs.gridx = 0;
        cs.gridy = 6;
        cs.gridwidth = 1;
        panel.add(lbStock, cs);

        tfStock = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 6;
        cs.gridwidth = 5;
        panel.add(tfStock, cs);

        lbSupplier = new JLabel("Proveedor: ");
        cs.gridx = 0;
        cs.gridy = 7;
        cs.gridwidth = 1;
        panel.add(lbSupplier, cs);

        tfSupplier = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 7;
        cs.gridwidth = 5;
        panel.add(tfSupplier, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnSubmit = new JButton("Aceptar");
        JButton btnErase = new JButton("Limpiar");

        JPanel bp = new JPanel();
        bp.add(btnSubmit);
        bp.add(btnErase);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        btnSubmit.addActionListener(e -> {
            try {
                String success = ProductController.update(getTfId(), getTfProduct(), getTfBuyCost(), getTfSellPrice(), getTfInventory(), getTfStock(), getTfSupplier());


                if (success.equals("supplier")) {
                    JOptionPane.showMessageDialog(ProductEdit.this,
                            "El proveedor no existe",
                            "Agregar Producto",
                            JOptionPane.ERROR_MESSAGE);
                } else if (success.equals("true")) {
                    JOptionPane.showMessageDialog(ProductEdit.this,
                            "Producto editado exitosamente",
                            "Agregar Producto",
                            JOptionPane.ERROR_MESSAGE);
                    clearFields();
                } else if (success.equals("false")) {
                    JOptionPane.showMessageDialog(ProductEdit.this,
                            "Valida la información",
                            "Agregar Producto",
                            JOptionPane.ERROR_MESSAGE);
                    clearFields();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });
        pack();
        //setSize(400, 400);
        setLocation(400, 0);
    }


    public String getTfProduct() {
        return tfProduct.getText();
    }

    public String getTfSupplier() {
        return tfSupplier.getText();
    }

    public int getTfId() {
        return Integer.parseInt(tfId.getText());
    }

    public double getTfSellPrice() {
        return Double.parseDouble(tfSellPrice.getText());
    }

    public int getTfStock() {
        return Integer.parseInt(tfStock.getText());
    }

    public double getTfBuyCost() {
        return Double.parseDouble(tfBuyCost.getText());
    }

    public int getTfInventory() {
        return Integer.parseInt(tfInventory.getText());
    }

    private void clearFields() {
        tfId.setText("");
        tfProduct.setText("");
        tfInventory.setText("");
        tfSellPrice.setText("");
        tfBuyCost.setText("");
        tfStock.setText("");
        tfSupplier.setText("");
    }
}
