package view.product_layout;

import controller.ProductController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;

public class ProductView extends JPanel {
    ProductView() {
        super(new GridLayout(1, 0));

        JTable table = null;
        try {
            table = new JTable(new ProductView.MyTableModel());
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_ALL_COLUMNS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        table.setPreferredScrollableViewportSize(new Dimension(1800, 1100));
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }

    class MyTableModel extends AbstractTableModel {
        private String[] columnNames = {"Id", "Producto", "PC", "PV", "Existencias", "NR", "St"};

        private Object[][] data = ProductController.read();

        MyTableModel() throws Exception {
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return 20;//data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }




        public boolean isCellEditable(int row, int col) {

            return true;
        }




        @Override
        public void fireTableCellUpdated(int row, int column) {
            super.fireTableCellUpdated(row, column);
        }


    }


    public static void createAndShowGUI(int x, int y) {
        JFrame frame = new JFrame("Productos");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ProductView newContentPane = new ProductView();
        //newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        frame.setLocation(x, y);
        frame.setSize(800, 400);
        // frame.pack();
        frame.setVisible(true);
        frame.revalidate();
        frame.repaint();
    }
}
