package view;


import view.components.Login;
import view.components.MyFrame;

class Main {
    public static void main(String[] args) {
        MyFrame screen = new MyFrame();

        Login loginDlg = new Login(screen);
        loginDlg.setVisible(true);
        if (loginDlg.isSucceeded()) {
            screen.setVisible(true);
            loginDlg.dispose();
        } else {
            System.out.print("Error");
        }


    }

}
