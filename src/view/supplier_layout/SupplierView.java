package view.supplier_layout;

import controller.ProductController;
import controller.SupplierController;
import model.DbConnection;
import model.SupplierModel;
import view.product_layout.ProductView;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;

public class SupplierView extends JPanel {
    SupplierView() {
        super(new GridLayout(1, 0));

        JTable table = null;
        try {
            table = new JTable(new SupplierView.MyTableModel());
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
            table.sizeColumnsToFit(JTable.AUTO_RESIZE_ALL_COLUMNS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        table.setPreferredScrollableViewportSize(new Dimension(1800, 1100));
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }

    class MyTableModel extends AbstractTableModel {
        private String[] columnNames = {"Id", "Compañia", "Teléfono", "Domicilio"};

        private Object[][] data = SupplierController.read();

        MyTableModel() throws Exception {
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return 20;//data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }


//        public Class getColumnClass(int c) {
//            return getValueAt(0, c).getClass();
//        }


        public boolean isCellEditable(int row, int col) {

            return true;
        }


//        public void setValueAt(Object value, int row, int col) {
//            int[] idArray;
//            int id;
//            data[row][col] = value;
//            try {
//                idArray = DbConnection.getId();
//            } catch (Exception e) {
//                e.printStackTrace();
//                idArray = null;
//            }
//            id = idArray[row];
//            fireTableCellUpdated(row, col);
//
//
//        }

        @Override
        public void fireTableCellUpdated(int row, int column) {
            super.fireTableCellUpdated(row, column);
        }


    }


    public static void createAndShowGUI(int x, int y) {
        JFrame frame = new JFrame("Proveedores");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SupplierView newContentPane = new SupplierView();
        //newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        frame.setLocation(x, y);
        frame.setSize(800, 400);
        // frame.pack();
        frame.setVisible(true);

    }
}
