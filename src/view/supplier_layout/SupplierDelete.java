package view.supplier_layout;

import controller.ProductController;
import controller.SupplierController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SupplierDelete extends JDialog {


    private JLabel lbId;
    private JTextField tfId;

    private JButton btnDelete;
    private JButton btnCancel;


    public SupplierDelete(Frame parent) {
        super(parent, "Eliminar proveedor", true);
        JPanel panel = new JPanel(new GridLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        lbId = new JLabel("Id");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbId, cs);

        tfId = new JTextField();
        cs.gridx = 1;
        cs.gridy = 0;
        panel.add(tfId, cs);


        btnDelete = new JButton("Eliminar");
        btnCancel = new JButton("Cancelar");

        btnDelete.addActionListener(e -> {
            SupplierController.delete(getId());
            dispose();
        });

        btnCancel.addActionListener(e -> dispose());

        JPanel bp = new JPanel();
        bp.add(btnDelete);
        bp.add(btnCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public int getId() {
        return Integer.parseInt(tfId.getText());
    }


}

