package view.supplier_layout;

import controller.ClientController;
import controller.SupplierController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SupplierAdd extends JDialog {
    private JTextField tfName;
    private JTextField tfPhone;
    private JTextField tfAddress;

    private JLabel lbName;
    private JLabel lbPhone;
    private JLabel lbAddress;


    public SupplierAdd(Frame parent) {
        super(parent, "Add", true);
        int i = 7;
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;

        lbName = new JLabel("Nombre: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfName, cs);

        lbPhone = new JLabel("Teléfono: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbPhone, cs);

        tfPhone = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 5;
        panel.add(tfPhone, cs);

        lbAddress = new JLabel("Domicilio: ");
        cs.gridx = 0;
        cs.gridy = 4;
        cs.gridwidth = 1;
        panel.add(lbAddress, cs);

        tfAddress = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 4;
        cs.gridwidth = 5;
        panel.add(tfAddress, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnSubmit = new JButton("Aceptar");
        JButton btnErase = new JButton("Limpiar");

        JPanel bp = new JPanel();
        bp.add(btnSubmit);
        bp.add(btnErase);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);


        btnSubmit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    SupplierController.create(getTfName(), getTfPhone(),
                            getTfAddress());
                    clearFields();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        });
        pack();
        setSize(400, 400);
        setLocationRelativeTo(null);
    }


    public String getTfName() {
        return tfName.getText();
    }

    public String getTfPhone() {
        return tfPhone.getText();
    }

    public String getTfAddress() {
        return tfAddress.getText();
    }


    private void clearFields() {
        tfPhone.setText("");
        tfName.setText("");
        tfAddress.setText("");

    }
}
