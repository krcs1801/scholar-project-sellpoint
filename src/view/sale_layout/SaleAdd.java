package view.sale_layout;

import controller.ProductController;
import controller.SaleController;
import view.product_layout.ProductEdit;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;


public class SaleAdd extends JDialog {
    int count = 1;
    private JLabel lbName;
    private JLabel lbProduct;
    private JLabel lbQuantity;
    private JTextField tfName;
    private JTextField tfProduct1;
    private JTextField tfQuantity1;

    int count1 = 4;


    public SaleAdd(Frame parent) {
        super(parent, "Add", true);
        int i = 7;
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;

        lbName = new JLabel("Cliente ID : ");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName = new JTextField(20);
        cs.gridx = 2;
        cs.gridy = 0;
        cs.gridwidth = 2;
        panel.add(tfName, cs);

        lbProduct = new JLabel("Producto ID");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(lbProduct, cs);

        lbQuantity = new JLabel("Cantitdad");
        cs.gridx = 2;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(lbQuantity, cs);


        tfProduct1 = new JTextField(20);
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 2;
        panel.add(tfProduct1, cs);

        tfQuantity1 = new JTextField(20);
        cs.gridx = 3;
        cs.gridy = 3;
        cs.gridwidth = 2;
        panel.add(tfQuantity1, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnAdd = new JButton("Añadir producto");
        JButton btnSubmit = new JButton("Finalizar");
        JButton btnErase = new JButton("Limpiar");

        JPanel bp = new JPanel();

        bp.add(btnAdd);
        bp.add(btnSubmit);
        bp.add(btnErase);
        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);
        btnAdd.addActionListener(e -> {

            if (SaleController.validProduct(tfProduct1.getText())) {
                JLabel label = new JLabel("" + tfProduct1.getText());
                cs.gridx = 0;
                cs.gridy = count1;
                cs.gridwidth = 2;
                panel.add(label, cs);


                JLabel label1 = new JLabel("" + tfQuantity1.getText());
                cs.gridx = 2;
                cs.gridy = count1;
                cs.gridwidth = 2;
                panel.add(label1, cs);
                panel.revalidate();
                panel.repaint();
                count1 += 1;
                SaleController.addToSale(tfProduct1.getText(), Integer.parseInt(tfQuantity1.getText()));
            } else {
                JOptionPane.showMessageDialog(SaleAdd.this,
                        "El producto no existe o esta mal escrito!",
                        "Agregar Producto",
                        JOptionPane.ERROR_MESSAGE);
                clearFields();
            }

        });

        btnSubmit.addActionListener(e -> {
            try {

                String total = SaleController.closeSale(Integer.parseInt(tfName.getText()));
                JOptionPane.showMessageDialog(SaleAdd.this,
                        total,
                        "Finalizar venta",
                        JOptionPane.INFORMATION_MESSAGE);
                clearFields();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        });
        pack();
        setSize(500, 400);
        setLocationRelativeTo(null);
    }


    public String getTfName() {
        return tfName.getText();
    }

    public void getproducts() {


        SaleController.addToSale(tfProduct1.getText(), Integer.parseInt(tfQuantity1.getText()));

    }


    private void clearFields() {
        tfProduct1.setText("");
        tfQuantity1.setText("");


    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        super.setDefaultCloseOperation(operation);
    }
}
