package view.user_layout;

import controller.ClientController;
import controller.UserController;
import view.sale_layout.SaleAdd;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class UserAdd extends JDialog {
    private JTextField tfName;
    private JTextField tfPassword;

    private JLabel lbName;
    private JLabel lbPassword;


    public UserAdd(Frame parent) {
        super(parent, "Add", true);
        int i = 7;
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;

        lbName = new JLabel("Nombre de usuario: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfName, cs);

        lbPassword = new JLabel("Contraseña: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbPassword, cs);

        tfPassword = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 5;
        panel.add(tfPassword, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnSubmit = new JButton("Aceptar");
        JButton btnErase = new JButton("Limpiar");

        JPanel bp = new JPanel();
        bp.add(btnSubmit);
        bp.add(btnErase);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        btnErase.addActionListener(e -> clearFields());
        btnSubmit.addActionListener(e -> {
            try {
                UserController.create(getTfName(), getTfPassword());
                JOptionPane.showMessageDialog(UserAdd.this,
                        "Usuario creado satisfactoriamiente!",
                        "Agregar Usuario",
                        JOptionPane.ERROR_MESSAGE);
                dispose();
                clearFields();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            clearFields();
        });
        pack();
        setSize(400, 400);
        setLocationRelativeTo(null);
    }


    public String getTfName() {
        return tfName.getText();
    }

    public String getTfPassword() {
        return tfPassword.getText();
    }


    private void clearFields() {
        tfPassword.setText("");
        tfName.setText("");

    }
}
