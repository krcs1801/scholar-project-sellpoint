package view.user_layout;

import controller.UserController;
import view.sale_layout.SaleAdd;

import javax.swing.*;
import java.awt.*;

public class UserDelete extends JDialog {
    private JLabel lbId;
    private JTextField tfId;

    private JButton btnDelete;
    private JButton btnCancel;


    public UserDelete(Frame parent) {
        super(parent, "Eliminar usuario", true);
        JPanel panel = new JPanel(new GridLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        lbId = new JLabel("Id");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbId, cs);

        tfId = new JTextField();
        cs.gridx = 1;
        cs.gridy = 0;
        panel.add(tfId, cs);


        btnDelete = new JButton("Eliminar");
        btnCancel = new JButton("Cancelar");

        btnDelete.addActionListener(e -> {
            UserController.delete(getId());
            JOptionPane.showMessageDialog(UserDelete.this,
                    "El usuario ah sido elimina satisfactoriamente!",
                    "Eliminar usuario",
                    JOptionPane.INFORMATION_MESSAGE);
        });

        btnCancel.addActionListener(e -> dispose());

        JPanel bp = new JPanel();
        bp.add(btnDelete);
        bp.add(btnCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public int getId() {
        return Integer.parseInt(tfId.getText());
    }


}
