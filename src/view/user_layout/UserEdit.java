package view.user_layout;


import controller.UserController;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class UserEdit extends JDialog {
    private JTextField tfPassword;
    private JTextField tfId;
    private JTextField tfName;

    private JLabel lbPassword;
    private JLabel lbId;
    private JLabel lbName;


    public UserEdit(Frame parent) {
        super(parent, "Editar", true);
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();


        cs.fill = GridBagConstraints.HORIZONTAL;
        lbId = new JLabel("Id: ");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbId, cs);

        tfId = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(tfId, cs);

        lbName = new JLabel("Nombre: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfName, cs);

        lbPassword = new JLabel("Contraseña: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbPassword, cs);

        tfPassword = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 2;
        panel.add(tfPassword, cs);


        panel.setBorder(new LineBorder(Color.GRAY));
        JButton btnSubmit = new JButton("Aceptar");
        JButton btnErase = new JButton("Cancelar");

        JPanel bp = new JPanel();
        bp.add(btnSubmit);
        bp.add(btnErase);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);
        btnErase.addActionListener(e -> {
            clearFields();
            dispose();
        });
        btnSubmit.addActionListener(e -> {
            UserController.update(getId(), getTfName(), getTfPassword());
            clearFields();
        });
        pack();
        setSize(400, 400);
        setLocation(500, 0);
    }


    public String getTfPassword() {

        return tfPassword.getText();
    }

    public String getTfName() {
        return tfName.getText();
    }


    public int getId() {
        return Integer.parseInt(tfId.getText());
    }


    void clearFields() {
        tfId.setText("");
        tfName.setText("");
        tfPassword.setText("");
    }

}
