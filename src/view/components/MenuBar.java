package view.components;


import model.ClientModel;
import view.client_layout.ClientAdd;
import view.client_layout.ClientDelete;
import view.client_layout.ClientEdit;
import view.client_layout.ClientView;
import view.product_layout.ProductAdd;
import view.product_layout.ProductDelete;
import view.product_layout.ProductEdit;
import view.product_layout.ProductView;
import view.sale_layout.SaleAdd;
import view.sale_layout.SaleDelete;
import view.sale_layout.SaleView;
import view.supplier_layout.SupplierAdd;
import view.supplier_layout.SupplierDelete;
import view.supplier_layout.SupplierEdit;
import view.supplier_layout.SupplierView;
import view.user_layout.UserAdd;
import view.user_layout.UserDelete;
import view.user_layout.UserEdit;
import view.user_layout.UserView;

import javax.swing.*;
import java.awt.*;

public class MenuBar extends JMenuBar {
    private JMenu products;
    private JMenu sales;
    private JMenu clients;
    private JMenu suppliers;
    private JMenu users;

    private JMenuItem mAddProduct;
    private JMenuItem mDeleteProduct;
    private JMenuItem mShowAllProducts;
    private JMenuItem mUpdateProduct;

    private JMenuItem mAddSale;
    private JMenuItem mDeleteSale;
    private JMenuItem mShowAllSales;

    private JMenuItem mAddClient;
    private JMenuItem mDeleteClient;
    private JMenuItem mShowAllClients;
    private JMenuItem mUpdateClient;

    private JMenuItem mAddSupplier;
    private JMenuItem mDeleteSupplier;
    private JMenuItem mShowAllSuppliers;
    private JMenuItem mUpdateSupplier;

    private JMenuItem mAddUser;
    private JMenuItem mDeleteUser;
    private JMenuItem mShowAllUsers;
    private JMenuItem mUpdateUser;


    MenuBar(Frame parent) {
        products = new JMenu("Almacen");
        sales = new JMenu("Ventas");
        clients = new JMenu("Clientes");
        suppliers = new JMenu("Proveedores");
        users = new JMenu("Usuario");

        mAddProduct = new JMenuItem("Añadir producto");
        mShowAllProducts = new JMenuItem("Mostrar todos los productos");
        mDeleteProduct = new JMenuItem("Eliminar producto");
        mUpdateProduct = new JMenuItem("Editar producto");

        mAddSale = new JMenuItem("Nueva venta");
        mShowAllSales = new JMenuItem("Lista de ventas");
        mDeleteSale = new JMenuItem("Cancelar venta");

        mAddSupplier = new JMenuItem("Añadir proveedor");
        mShowAllSuppliers = new JMenuItem("Mostrar todos los proveedores");
        mDeleteSupplier = new JMenuItem("Eliminar proveedor");
        mUpdateSupplier = new JMenuItem("Editar proveedor");

        mAddClient = new JMenuItem("Añadir cliente");
        mShowAllClients = new JMenuItem("Mostrar todos los clientes");
        mDeleteClient = new JMenuItem("Eliminar cliente");
        mUpdateClient = new JMenuItem("Editar cliente");

        mAddUser = new JMenuItem("Añadir usuario");
        mShowAllUsers = new JMenuItem("Mostrar todos los usuarios");
        mDeleteUser = new JMenuItem("Eliminar usuario");
        mUpdateUser = new JMenuItem("Editar usuario");


        mAddProduct.addActionListener(e -> {
            ProductAdd productAdd = new ProductAdd(parent);
            productAdd.setVisible(true);
        });
        mShowAllProducts.addActionListener(e -> ProductView.createAndShowGUI(370, 500));

        mUpdateProduct.addActionListener(e -> {
            ProductEdit productEdit = new ProductEdit(parent);
            ProductView.createAndShowGUI(370, 500);
            productEdit.setVisible(true);


        });
        mDeleteProduct.addActionListener(e -> {
            ProductDelete productDelete = new ProductDelete(parent);
            ProductView.createAndShowGUI(370, 500);
            productDelete.setVisible(true);
        });

        mAddSale.addActionListener(e -> {
            SaleAdd saleAdd = new SaleAdd(parent);
            ProductView.createAndShowGUI(370, 500);
            ClientView.createAndShowGUI(0, 500);
            saleAdd.setVisible(true);

        });
        mShowAllSales.addActionListener(e -> SaleView.createAndShowGUI(370, 500));
        mDeleteSale.addActionListener(e -> {
            SaleDelete saleDelete = new SaleDelete(parent);
            saleDelete.setVisible(true);
            SaleView.createAndShowGUI(500, 500);
        });

        mAddClient.addActionListener(e -> {
            ClientAdd clientAdd = new ClientAdd(parent);

            clientAdd.setVisible(true);
        });
        mShowAllClients.addActionListener(e -> ClientView.createAndShowGUI(370, 500));
        mUpdateClient.addActionListener(e -> {
            ClientEdit clientEdit = new ClientEdit(parent);
            ClientView.createAndShowGUI(370, 500);
            clientEdit.setVisible(true);
        });
        mDeleteClient.addActionListener(e -> {
            ClientDelete clientDelete = new ClientDelete(parent);
            ClientView.createAndShowGUI(370, 500);
            clientDelete.setVisible(true);
        });

        mAddUser.addActionListener(e -> {
            UserAdd userAdd = new UserAdd(parent);
            userAdd.setVisible(true);
        });
        mShowAllUsers.addActionListener(e -> UserView.createAndShowGUI(370, 500));
        mUpdateUser.addActionListener(e -> {
            UserEdit userEdit = new UserEdit(parent);
            UserView.createAndShowGUI(370, 500);
            userEdit.setVisible(true);
        });
        mDeleteUser.addActionListener(e -> {
            UserDelete userDelete = new UserDelete(parent);
            UserView.createAndShowGUI(370, 500);
            userDelete.setVisible(true);
        });

        mAddSupplier.addActionListener(e -> {
            SupplierAdd supplierAdd = new SupplierAdd(parent);

            supplierAdd.setVisible(true);
        });
        mShowAllSuppliers.addActionListener(e -> SupplierView.createAndShowGUI(370, 500));
        mUpdateSupplier.addActionListener(e -> {
            SupplierEdit supplierEdit = new SupplierEdit(parent);
            SupplierView.createAndShowGUI(370, 500);
            supplierEdit.setVisible(true);
        });
        mDeleteSupplier.addActionListener(e -> {
            SupplierDelete supplierDelete = new SupplierDelete(parent);
            ClientView.createAndShowGUI(370, 500);
            supplierDelete.setVisible(true);
        });


        add(products);
        products.add(mAddProduct);
        products.add(mShowAllProducts);
        products.add(mUpdateProduct);
        products.add(mDeleteProduct);
        add(sales);
        sales.add(mAddSale);
        sales.add(mShowAllSales);
        sales.add(mDeleteSale);
        add(clients);
        clients.add(mAddClient);
        clients.add(mShowAllClients);
        clients.add(mUpdateClient);
        clients.add(mDeleteClient);
        add(suppliers);
        suppliers.add(mAddSupplier);
        suppliers.add(mShowAllSuppliers);
        suppliers.add(mUpdateSupplier);
        suppliers.add(mDeleteSupplier);
        add(users);
        users.add(mAddUser);
        users.add(mShowAllUsers);
        users.add(mUpdateUser);
        users.add(mDeleteUser);

    }
}
