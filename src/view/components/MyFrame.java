package view.components;

import javax.swing.*;

public class MyFrame extends JFrame {
    MenuBar menuBar;

    public MyFrame() {


        menuBar = new MenuBar(this);
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(false);
        //setLocationRelativeTo(null);
        setJMenuBar(menuBar);
        setSize(800, 400);


    }
}
