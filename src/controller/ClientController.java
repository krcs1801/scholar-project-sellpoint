package controller;

import model.ClientModel;
import java.util.Calendar;

public class ClientController {


    public ClientController() {
    }

    static boolean client(String client) {
        boolean exists;
        exists = ClientModel.getClient(client);
        return exists;
    }
    public static boolean create(String name, String phone, String address)  {
        String today = Calendar.getInstance().getTime().toString();

        boolean success = ClientModel.create(name, phone, address, today);
        return success;
    }

    public static String[][] read(){

        String[][] arr = ClientModel.read();
        return arr;
    }

    public static boolean update(int id,String name, String phone, String address) throws Exception {
        String today = Calendar.getInstance().getTime().toString();
        boolean success = ClientModel.update(id,name, phone, address,today);
        return success;
    }

    public static boolean delete(int id) {
        boolean success = ClientModel.delete(id);
        return success;
    }


}
