package controller;


import model.ProductModel;
import model.SaleModel;

import java.util.Calendar;

public class SaleController {

    private static double total = 0;
    private static int count = 0;
    private static String[] products = new String[50];
    private static int[] quantity = new int[50];

    private static boolean clientExists(String client) {
        boolean exists;
        exists = ClientController.client(client);
        return exists;
    }

    public static boolean validProduct(String p) {
        boolean exists = false;
        if (ProductController.product(p) > 0.0) {
            exists = true;
        }

        return exists;
    }

    public static void addToSale(String p, int q) {
        double price = ProductController.product(p);
        if (price > 0.0) {
            count += 1;
            products[count] = p;
            quantity[count] = q;
            System.out.print(products[count]);
            System.out.print(quantity[count]);
            total += price * q;
        }
    }


    public static String closeSale(int client) {
        double t = total;
        String today = Calendar.getInstance().getTime().toString();

        for (int j = 0; j < 50; j++) {

            ProductModel.updateInventory(products[j], quantity[j]);


        }
        SaleModel.create(client, total, today);
        total = 0;
        return "El total de la venta es de " + t;
    }


    public static String[][] read() {

        String[][] arr = SaleModel.read();
        return arr;
    }


    public static boolean delete(int id) {
         boolean success = SaleModel.delete(id);
        return success;
    }


}
