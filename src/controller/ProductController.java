package controller;

import model.ProductModel;


public class ProductController {

    static double product(String product) {
        double price;
        price = ProductModel.getProduct(product);
        return price;
    }

    private static boolean supplierExists(String supplier) {
        boolean exists;
        exists = SupplierController.supplier(supplier);
        return exists;
    }

    public static String create(String name, double bc, double sp, int stock, int nr, String supplier) {
        String success = "supplier";
        if (supplierExists(supplier)) {
            success = ProductModel.create(name, bc, sp, stock, nr, supplier);

        }
        return success;
    }

    public static String[][] read() throws Exception {

        String[][] arr = ProductModel.read();
        return arr;
    }

    public static String update(int id, String name, double bc, double sp, int stock, int nr, String supplier) throws Exception {
        String success = "supplier";
        if (supplierExists(supplier)) {
            success = ProductModel.update(id, name, bc, sp, stock, nr, supplier);
        }
        return success;
    }

    public static boolean delete(int id) {
        boolean success = ProductModel.delete(id);
        return success;
    }


}
