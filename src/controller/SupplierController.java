package controller;

import model.SupplierModel;

import java.sql.ResultSet;

public class SupplierController {

    static boolean supplier(String supplier) {
        boolean exists;
        exists = SupplierModel.getSupplier(supplier);
        return exists;
    }

    public static boolean create( String name, String phone, String address) {

        boolean success = SupplierModel.create(name, phone, address);
        return success;
    }

    public static String[][] read() throws Exception {

        String[][] arr = SupplierModel.read();
        return arr;
    }

    public static boolean update(int id, String name, String phone, String address) throws Exception {
        boolean success = SupplierModel.update(id, name, phone, address);
        return success;
    }

    public static boolean delete(int id) {
        boolean success = SupplierModel.delete(id);
        return success;
    }

}
