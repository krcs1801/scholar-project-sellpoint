package controller;

import model.ClientModel;
import model.UserModel;

import java.sql.ResultSet;

import static model.UserModel.authenticate;

public class UserController {


    public static boolean auth(String username, String password) throws Exception {
        boolean success = authenticate(username, password);
        return success;
    }

    public static boolean create(String username, String password) {

        boolean success = UserModel.create(username, password);
        return success;
    }

    public static String[][] read() {

        String[][] arr = UserModel.read();
        return arr;
    }

    public static boolean update(int id, String username, String password) {
        boolean success = UserModel.update(id, username, password);
        return success;
    }

    public static boolean delete(int id) {
        boolean success = UserModel.delete(id);
        return success;
    }

}
