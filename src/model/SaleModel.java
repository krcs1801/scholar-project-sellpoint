package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static model.DbConnection.connectionDB;

public class SaleModel {

    public static boolean delete(int id) {
        boolean success;
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement("DELETE FROM sellpoint.sale WHERE Id = '" + id + "';");
            statement.executeUpdate();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public static String[][] read() {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM sellpoint.sale");
            ResultSet result = statement.executeQuery();
            String[][] array = new String[10000][4];
            int count = 0;
            while (result.next()) {
                array[count][0] = result.getString("Id");
                array[count][1] = result.getString("Client");
                array[count][2] = result.getString("Total");
                array[count][3] = result.getString("Date_");
                count += 1;
            }
            return array;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;
        }
    }

    public static String create(int client, double total, String date) {
        try {
            Connection con = connectionDB();
            PreparedStatement insertClient = con.prepareStatement("INSERT INTO " +
                    "sale(Client,Total,Date_)VALUES" +
                    "('" + client + "','" + total + "','" + date + "')");
            insertClient.executeUpdate();
            return "true";
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return "false";
        }
    }

}
