package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DbConnection {
    protected static Connection connectionDB() throws Exception {
        try {
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/sellpoint?useSSL=false";
            String username = "";
            String password = "";
            Class.forName(driver);
            Connection con = DriverManager.getConnection(url, username, password);
            return con;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;

        }
    }
    public static int[] getId() throws Exception {
        Connection con = connectionDB();
        PreparedStatement statement = con.prepareStatement("SELECT id FROM sellpoint.product");
        ResultSet result = statement.executeQuery();
        int idArray[] = new int[10000];
        int count = 0;
        while (result.next()) {

            idArray[count] = result.getInt("id");
            count++;
        }
        return idArray;
    }
}
