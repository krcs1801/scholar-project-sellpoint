package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static model.DbConnection.connectionDB;

public class SupplierModel {
    public static boolean delete(int id) {
        boolean success;
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement("DELETE FROM sellpoint.supplier WHERE id = '" + id + "';");
            statement.executeUpdate();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public static String[][] read() throws Exception {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM sellpoint.supplier");
            ResultSet result = statement.executeQuery();
            String[][] array = new String[10000][4];
            int count = 0;
            while (result.next()) {
                array[count][0] = result.getString("Id");
                array[count][1] = result.getString("Company");
                array[count][2] = result.getString("Phone");
                array[count][3] = result.getString("Address");
                count += 1;
            }
            //  System.out.print(array[0][1]);
            return array;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;
        }

    }

    public static boolean create(String company, String phone, String address) {
        try {
            Connection con = connectionDB();
            PreparedStatement insertSupplier = con.prepareStatement("INSERT INTO " +
                    "supplier(Company,Phone,Address)VALUES" +
                    "('" + company + "','" + phone + "','" + address + "')");
            insertSupplier.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return false;
        }
    }

    public static boolean update(int id, String company, String phone, String address) throws Exception {
        boolean success;
        try {
            Connection con = DbConnection.connectionDB();
            String query = "UPDATE sellpoint.supplier SET Company='" + company + "',Phone='" + phone + "',Address='" + address + "' WHERE id='" + id + "'";
            PreparedStatement statement = con.prepareStatement(query);
            statement.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            success = false;
        }
        return success;
    }

    public static boolean getSupplier(String supplier) {
        boolean isValid = false;
        try {
            Connection con = DbConnection.connectionDB();
            String query = "SELECT Company FROM sellpoint.supplier WHERE Company='" + supplier + "'";
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            result.next();
            if (result.getString("Company").length() >= 1) {
                isValid = true;
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        }
        return isValid;

    }
}