package model;

import model.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static model.DbConnection.connectionDB;

public class UserModel {
    public static boolean authenticate(String username, String password) throws Exception {
        boolean isValid = false;
        try {
            Connection con = DbConnection.connectionDB();
            String query = "SELECT Username FROM sellpoint.user WHERE Username='" + username + "' AND Password='" + password + "'";
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            result.next();
            if (result.getString("Username").length() >= 1) {
                isValid = true;
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        }
        return isValid;
    }

    public static boolean create(String username, String password) {
        try {
            Connection con = DbConnection.connectionDB();
            PreparedStatement insertProduct = con.prepareStatement("INSERT INTO " +
                    "user(Username,Password)VALUES" +
                    "('" + username + "','" + password + "')");
            insertProduct.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("Error " + e);
            return false;
        }
    }

    public static boolean update(int id, String username, String password) {
        try {
            Connection con = DbConnection.connectionDB();
            String query = "UPDATE sellpoint.user SET username='" + username + "',password='" + password + "' WHERE id='" + id + "'";
            PreparedStatement statement = con.prepareStatement(query);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("Error " + e);
            return false;
        }
    }

    public static boolean delete(int id) {
        try {
            Connection con = DbConnection.connectionDB();
            String query = "DELETE FROM sellpoint.user WHERE Id='" + id + "'";
            PreparedStatement statement = con.prepareStatement(query);
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("Error " + e);
            return false;
        }
    }

    public static String[][] read() {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM sellpoint.user");
            ResultSet result = statement.executeQuery();
            String[][] array = new String[10000][2];
            int count = 0;
            while (result.next()) {
                array[count][0] = result.getString("Id");
                array[count][1] = result.getString("Username");
                count += 1;
            }
            //  System.out.print(array[0][1]);
            return array;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;
        }
    }


}

