package model;

public abstract class Model {
    public abstract boolean create();

    abstract boolean update();

    abstract boolean read();

    abstract boolean delete();

}
