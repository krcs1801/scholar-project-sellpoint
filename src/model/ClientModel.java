package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static model.DbConnection.connectionDB;

public class ClientModel {

    public static boolean delete(int id) {
        boolean success;
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement("DELETE FROM sellpoint.client WHERE Id = '" + id + "';");
            statement.executeUpdate();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public static String[][] read() {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM sellpoint.client");
            ResultSet result = statement.executeQuery();
            String[][] array = new String[10000][5];
            int count = 0;
            while (result.next()) {
                array[count][0] = result.getString("Id");
                array[count][1] = result.getString("C_name");
                array[count][2] = result.getString("Phone");
                array[count][3] = result.getString("Address");
                array[count][4] = result.getString("Last_visit");
                count += 1;
            }
            //  System.out.print(array[0][1]);
            return array;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;
        }
    }

    public static boolean create(String name, String phone, String address, String lastVisit) {
        try {
            Connection con = connectionDB();
            PreparedStatement insertClient = con.prepareStatement("INSERT INTO " +
                    "client(C_name,Phone,Address,Last_visit)VALUES" +
                    "('" + name + "','" + phone + "','" + address + "','" + lastVisit + "')");
            insertClient.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return false;
        }
    }

    public static boolean update(int id, String name, String phone, String address, String lastVisit) throws Exception {
        boolean success;
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "UPDATE client SET C_name='" + name + "',Phone='" + phone + "',Address='" + address + "',Last_visit'" + lastVisit + "' WHERE Id='" + id + "')");
            statement.executeUpdate();
            success = true;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            success = false;
        }
        return success;
    }

    public static boolean getClient(String client) {
        boolean isValid = false;
        try {
            Connection con = DbConnection.connectionDB();
            String query = "SELECT C_name FROM sellpoint.client WHERE Company='" + client + "'";
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            result.next();
            if (result.getString("C_name").length() >= 1) {
                isValid = true;
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        }
        return isValid;

    }
}
