package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static model.DbConnection.connectionDB;

public class ProductModel {


    public static boolean delete(int id) {
        boolean success;
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement("DELETE FROM sellpoint.product WHERE Id = '" + id + "';");
            statement.executeUpdate();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public static String[][] read() throws Exception {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM sellpoint.product");
            ResultSet result = statement.executeQuery();
            String[][] array = new String[10000][7];
            int count = 0;
            while (result.next()) {
                array[count][0] = result.getString("Id");
                array[count][1] = result.getString("Product");
                array[count][2] = result.getString("Buy_cost");
                array[count][3] = result.getString("Sell_price");
                array[count][4] = result.getString("Stock");
                array[count][5] = result.getString("Reorder_level");
                array[count][6] = result.getString("Supplier");
                count += 1;
            }
            return array;
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return null;
        }

    }

    public static String create(String product, double bc, double sp, int stock, int nr, String supplier) {
        try {
            Connection con = connectionDB();
            PreparedStatement insertProduct = con.prepareStatement("INSERT INTO " +
                    "product(Product,Buy_cost,Sell_price,Stock,Reorder_level,Supplier)VALUES" +
                    "('" + product + "','" + bc + "','" + sp + "','" + stock + "','" + nr + "','" + supplier + "')");
            insertProduct.executeUpdate();
            return "true";
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return "false";
        }
    }

    public static String update(int id, String product, double bc, double sp, int stock, int rl, String supplier) throws Exception {
        try {
            Connection con = connectionDB();
            PreparedStatement statement = con.prepareStatement(
                    "UPDATE sellpoint.product SET Product = '" + product + "', Buy_cost='" + bc + "',Sell_price='" + sp + "',Stock='" + stock + "',Reorder_level='" + rl + "',Supplier='" + supplier + "' WHERE Id='" + id + "'");
            statement.executeUpdate();
            return "true";
        } catch (Exception ex) {
            System.out.println("Error " + ex);
            return "false";
        }

    }

    public static double getProduct(String product) {
        double price = 0.0;
        try {
            Connection con = DbConnection.connectionDB();
            String query = "SELECT Sell_price,Product FROM sellpoint.product WHERE Product='" + product + "'";
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            result.next();
            if (result.getString("Product").length() >= 1) {
                price = result.getDouble("Sell_price");
            }

        } catch (Exception e) {
            System.out.println("Error " + e);
        }
        return price;

    }

    public static void updateInventory(String product, int quantity) {
        try {
            Connection con = connectionDB();
            int i = 0;

            String query = "SELECT Stock,Product FROM sellpoint.product WHERE Product='" + product + "'";
            PreparedStatement stock = con.prepareStatement(query);
            ResultSet result = stock.executeQuery();
            result.next();
            if (result.getString("Product").length() >= 1) {
                i = result.getInt("Stock");
            }

            i -= quantity;
            PreparedStatement statement = con.prepareStatement(
                    "UPDATE sellpoint.product SET Stock='" + i + "' WHERE Product='" + product + "'");
            statement.executeUpdate();
        } catch (Exception ex) {
            System.out.println("Error " + ex);
        }
    }

}
